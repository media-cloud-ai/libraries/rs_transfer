use clap::Parser;
use onedrive_api::{Auth, Permission};
use rs_transfer::Error;
use std::io::{self, Write as _};

#[tokio::main]
async fn main() -> Result<(), Error> {
  #[derive(Parser, Debug)]
  #[clap(author, version, about, long_about = None)]
  struct Args {
    #[clap(short, long)]
    client_id: String,
    #[clap(
      short,
      long,
      default_value = "https://login.microsoftonline.com/common/oauth2/nativeclient"
    )]
    redirect_uri: String,
  }

  let args = Args::parse();

  let auth = Auth::new(
    &args.client_id,
    Permission::new_read().write(true).offline_access(true),
    &args.redirect_uri,
  );

  let url = auth.code_auth_url();

  if open::that(url).is_err() {
    println!("Cannot open browser, please open the url above manually.");
  }

  eprintln!("Please login in browser, paste the redirected URL here and then press <Enter>");
  let code = loop {
    eprint!("Redirected URL: ");
    io::stdout().flush()?;
    let mut inp = String::new();
    io::stdin().read_line(&mut inp)?;
    let inp = inp.trim();

    const NEEDLE: &str = "code=";
    match inp.find(NEEDLE) {
      Some(pos) => break inp[pos + NEEDLE.len()..].to_owned(),
      _ => eprintln!("Invalid!"),
    }
  };

  let token = auth.login_with_code(&code, None).await?;
  println!("token: {:?}", token.access_token);

  Ok(())
}
