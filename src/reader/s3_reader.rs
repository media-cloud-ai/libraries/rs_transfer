use crate::{
  endpoint::S3Endpoint,
  error::Error,
  reader::{ReaderNotification, StreamReader},
  StreamData,
};
use async_std::channel::Sender;
use async_trait::async_trait;
use rusoto_s3::{GetObjectRequest, HeadObjectRequest, S3Client, S3};
use std::{io::Read, sync::Arc};

impl S3Endpoint {
  async fn read_file(
    &self,
    client: Arc<S3Client>,
    path: &str,
    bucket: &str,
    sender: Sender<StreamData>,
    channel: &dyn ReaderNotification,
  ) -> Result<u64, Error> {
    let mut total_read_bytes: u64 = 0;
    let head_request = HeadObjectRequest {
      bucket: bucket.to_string(),
      key: path.to_string(),
      ..Default::default()
    };

    let request = GetObjectRequest {
      bucket: bucket.to_string(),
      key: path.to_string(),
      ..Default::default()
    };

    let cloned_client = client.clone();
    let handler = self
      .runtime()
      .lock()
      .unwrap()
      .spawn(async move { cloned_client.head_object(head_request).await });
    let head = handler.await??;

    if let Some(size) = head.content_length {
      sender.send(StreamData::Size(size as u64)).await?;
    }

    let handler = self
      .runtime()
      .lock()
      .unwrap()
      .spawn(async move { client.get_object(request).await });

    let object = handler.await??;

    let s3_byte_stream = object
      .body
      .ok_or_else(|| Error::Other("No retrieved object data to access.".to_string()))?;
    let mut reader = s3_byte_stream.into_blocking_read();

    let buffer_size = if let Ok(buffer_size) = std::env::var("S3_READER_BUFFER_SIZE") {
      buffer_size
        .parse::<u32>()
        .map_err(|error| Error::from(("S3_READER_BUFFER_SIZE", error)))? as usize
    } else {
      1024 * 1024
    };

    loop {
      if channel.is_stopped() {
        sender.send(StreamData::Stop).await?;
        return Ok(total_read_bytes);
      }

      let mut buffer: Vec<u8> = vec![0; buffer_size];
      let size = reader.read(&mut buffer)?;
      total_read_bytes += size as u64;
      if size == 0 {
        break;
      }

      if let Err(error) = sender
        .send(StreamData::Data(buffer[0..size].to_vec()))
        .await
      {
        if channel.is_stopped() && sender.is_closed() {
          log::warn!("Data channel closed: could not send {} read bytes.", size);
          return Ok(total_read_bytes);
        }

        return Err(error.into());
      }
    }
    sender.send(StreamData::Eof).await?;
    Ok(total_read_bytes)
  }
}

#[async_trait]
impl StreamReader for S3Endpoint {
  async fn read_stream(
    &self,
    path: &str,
    sender: Sender<StreamData>,
    channel: &dyn ReaderNotification,
  ) -> Result<u64, Error> {
    let cloned_bucket = self.bucket().to_string();
    let cloned_path = path.to_string();
    let client = self.connection();

    self
      .read_file(client, &cloned_path, &cloned_bucket, sender, channel)
      .await
  }
}
