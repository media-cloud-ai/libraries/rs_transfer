mod cursor_reader;
mod file_reader;
mod ftp_reader;
mod gcs_reader;
mod http_reader;
mod one_drive_reader;
mod s3_reader;
mod sftp_reader;

use crate::error::Error;
use crate::StreamData;
use async_std::channel::Sender;
use async_trait::async_trait;

#[async_trait]
pub trait StreamReader {
  async fn read_stream(
    &self,
    path: &str,
    sender: Sender<StreamData>,
    channel: &dyn ReaderNotification,
  ) -> Result<u64, Error>;
}

pub trait ReaderNotification: Sync + Send {
  fn is_stopped(&self) -> bool {
    false
  }
}

pub struct SimpleReader {}

impl ReaderNotification for SimpleReader {}
