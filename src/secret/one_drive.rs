use crate::secret::Password;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Eq, JsonSchema, PartialEq, Serialize)]
pub struct OneDriveSecret {
  pub client_id: String,
  pub token: Password,
}

impl OneDriveSecret {
  pub fn client_id(&self) -> &str {
    &self.client_id
  }
  pub fn token(&self) -> &str {
    &self.token.0
  }
}
