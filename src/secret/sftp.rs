use crate::secret::Password;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Eq, JsonSchema, PartialEq, Serialize)]
pub struct SftpSecret {
  pub hostname: String,
  pub port: Option<u16>,
  pub username: String,
  pub password: Option<Password>,
  pub known_host: Option<Password>,
  pub root_directory: Option<String>,
}

impl SftpSecret {
  pub fn hostname(&self) -> &str {
    &self.hostname
  }

  pub fn port(&self) -> u16 {
    self.port.unwrap_or(22)
  }

  pub fn username(&self) -> &str {
    &self.username
  }

  pub fn password(&self) -> Option<String> {
    self.password.as_ref().map(|value| value.to_string())
  }

  pub fn known_host(&self) -> Option<String> {
    self.known_host.as_ref().map(|value| value.to_string())
  }

  pub fn root_directory(&self) -> &Option<String> {
    &self.root_directory
  }
}
