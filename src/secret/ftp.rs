use crate::secret::Password;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Eq, JsonSchema, PartialEq, Serialize)]
pub struct FtpSecret {
  pub hostname: String,
  pub port: Option<u16>,
  pub secure: Option<bool>,
  pub username: Option<String>,
  pub password: Option<Password>,
  pub root_directory: Option<String>,
}

impl FtpSecret {
  pub fn hostname(&self) -> &str {
    &self.hostname
  }

  pub fn port(&self) -> u16 {
    self.port.unwrap_or(21)
  }

  pub fn is_secure(&self) -> bool {
    self.secure.unwrap_or(false)
  }

  pub fn username(&self) -> &Option<String> {
    &self.username
  }

  pub fn password(&self) -> Option<String> {
    self.password.as_ref().map(|value| value.to_string())
  }

  pub fn root_directory(&self) -> &Option<String> {
    &self.root_directory
  }
}

#[test]
pub fn test_ftp_secret_getters() {
  let hostname = "ftp.server.name".to_string();
  let port = None;
  let secure = None;
  let username = Some("user".to_string());
  let password = Password::from("password");
  let root_directory = "/root/path".to_string();

  let secret = FtpSecret {
    hostname: hostname.clone(),
    port,
    secure,
    username: username.clone(),
    password: Some(password.clone()),
    root_directory: Some(root_directory.clone()),
  };

  println!("{secret:?}");

  assert_eq!(secret.hostname(), &hostname);
  assert_eq!(secret.port(), 21);
  assert!(!secret.is_secure());
  assert_eq!(secret.username(), &username);
  assert_eq!(secret.password(), Some(password.0));
  assert_eq!(secret.root_directory(), &Some(root_directory))
}
