use crate::secret::Password;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use std::io::{Error, ErrorKind};

#[derive(Clone, Debug, Deserialize, Eq, JsonSchema, PartialEq, Serialize)]
pub struct GcsSecret {
  pub credential: GcsCredential,
  pub bucket: String,
}

impl GcsSecret {
  pub fn credential(&self) -> &GcsCredential {
    &self.credential
  }
  pub fn bucket(&self) -> &str {
    &self.bucket
  }
}

#[derive(Clone, Debug, Deserialize, Eq, JsonSchema, PartialEq, Serialize)]
pub struct GcsCredential {
  #[serde(rename = "type")]
  pub gcs_type: String,
  pub project_id: String,
  pub private_key_id: Password,
  pub private_key: Password,
  pub client_email: String,
  pub client_id: String,
  pub auth_uri: String,
  pub token_uri: String,
  pub auth_provider_x509_cert_url: String,
  pub client_x509_cert_url: String,
}

impl GcsCredential {
  pub fn to_json(&self) -> Result<String, Error> {
    serde_json::to_string(&self).map_err(|error| Error::new(ErrorKind::Other, error.to_string()))
  }
}
