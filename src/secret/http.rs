use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Eq, JsonSchema, PartialEq, Serialize)]
pub struct HttpSecret {
  pub endpoint: Option<String>,
  pub method: Option<String>,
  pub headers: Option<String>,
  pub body: Option<String>,
}

impl HttpSecret {
  pub fn endpoint(&self) -> &Option<String> {
    &self.endpoint
  }

  pub fn method(&self) -> &Option<String> {
    &self.method
  }

  pub fn headers(&self) -> &Option<String> {
    &self.headers
  }

  pub fn body(&self) -> &Option<String> {
    &self.body
  }
}
