use schemars::JsonSchema;
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Deserialize, Eq, JsonSchema, PartialEq, Serialize)]
pub struct CursorSecret {
  pub content: Option<Vec<u8>>,
}

impl CursorSecret {
  pub fn content(&self) -> Vec<u8> {
    self.content.clone().unwrap_or_default()
  }
}
