use crate::{secret::Password, Error};
use rusoto_core::Region;
use schemars::JsonSchema;
use serde::{Deserialize, Serialize};
use std::str::FromStr;

#[derive(Clone, Debug, Deserialize, Eq, JsonSchema, PartialEq, Serialize)]
pub struct S3Secret {
  pub hostname: Option<String>,
  pub access_key_id: String,
  pub secret_access_key: Password,
  pub region: Option<String>,
  pub bucket: String,
}

impl S3Secret {
  pub fn hostname(&self) -> &Option<String> {
    &self.hostname
  }

  pub fn access_key(&self) -> &str {
    &self.access_key_id
  }

  pub fn secret_key(&self) -> &str {
    &self.secret_access_key.0
  }

  pub fn region_as_string(&self) -> &Option<String> {
    &self.region
  }

  pub fn bucket(&self) -> &str {
    &self.bucket
  }

  pub fn region(&self) -> Result<Region, Error> {
    match (self.hostname(), self.region_as_string()) {
      (Some(hostname), Some(region)) => Ok(Region::Custom {
        name: region.to_string(),
        endpoint: hostname.to_string(),
      }),
      (Some(hostname), None) => Ok(Region::Custom {
        name: Region::default().name().to_string(),
        endpoint: hostname.to_string(),
      }),
      (None, Some(region)) => {
        Region::from_str(region).map_err(|error| Error::from((region.as_str(), error)))
      }
      (None, None) => Ok(Region::default()),
    }
  }
}

#[test]
pub fn test_s3_secret_getters() {
  let hostname = Some("s3.server.name".to_string());
  let access_key_id = "S3_ACCESS_KEY".to_string();
  let secret_access_key = "S3_SECRET_KEY".to_string();
  let region = Some("s3-region".to_string());
  let bucket = "s3-bucket".to_string();

  let secret = S3Secret {
    hostname: hostname.clone(),
    access_key_id: access_key_id.clone(),
    secret_access_key: Password(secret_access_key.clone()),
    region: region.clone(),
    bucket: bucket.clone(),
  };

  println!("secret: {secret:?}");

  assert_eq!(secret.hostname(), &hostname);
  assert_eq!(secret.access_key(), &access_key_id);
  assert_eq!(secret.secret_key(), &secret_access_key);
  assert_eq!(secret.region_as_string(), &region);
  assert_eq!(secret.bucket(), &bucket);
}

#[test]
pub fn test_s3_secret_get_region() {
  let secret = S3Secret {
    hostname: Some("s3.server.name".to_string()),
    access_key_id: "s3_access_key".to_string(),
    secret_access_key: Password::from("s3_secret_key"),
    region: Some("s3-region".to_string()),
    bucket: "".to_string(),
  };

  let expected = Region::Custom {
    name: "s3-region".to_string(),
    endpoint: "s3.server.name".to_string(),
  };
  let region = secret.region().unwrap();
  assert_eq!(region, expected);
}

#[test]
pub fn test_s3_secret_get_region_no_hostname() {
  let secret = S3Secret {
    hostname: None,
    access_key_id: "s3_access_key".to_string(),
    secret_access_key: Password::from("s3_secret_key"),
    region: Some("eu-west-3".to_string()),
    bucket: "".to_string(),
  };

  let expected = Region::EuWest3;
  let region = secret.region().unwrap();
  assert_eq!(region, expected);
}

#[test]
pub fn test_s3_secret_get_region_no_hostname_and_invalid_region() {
  let secret = S3Secret {
    hostname: None,
    access_key_id: "s3_access_key".to_string(),
    secret_access_key: Password::from("s3_secret_key"),
    region: Some("s3-region".to_string()),
    bucket: "".to_string(),
  };

  let expected = "Unable to parse 's3-region' region: Not a valid AWS region: s3-region";
  let error = secret.region().unwrap_err();
  assert_eq!(error.to_string(), expected.to_string());
}

#[test]
pub fn test_s3_secret_get_region_no_region() {
  let secret = S3Secret {
    hostname: Some("s3.server.name".to_string()),
    access_key_id: "s3_access_key".to_string(),
    secret_access_key: Password::from("s3_secret_key"),
    region: None,
    bucket: "".to_string(),
  };

  let expected = Region::Custom {
    name: Region::default().name().to_string(),
    endpoint: "s3.server.name".to_string(),
  };
  let region = secret.region().unwrap();
  assert_eq!(region, expected);
}

#[test]
pub fn test_s3_secret_get_region_no_hostname_nor_region() {
  let secret = S3Secret {
    hostname: None,
    access_key_id: "s3_access_key".to_string(),
    secret_access_key: Password::from("s3_secret_key"),
    region: None,
    bucket: "".to_string(),
  };

  let expected = Region::default();
  let region = secret.region().unwrap();
  assert_eq!(region, expected);
}
