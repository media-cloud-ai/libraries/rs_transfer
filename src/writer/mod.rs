mod file_writer;
mod ftp_writer;
mod gcs_writer;
mod s3_writer;
mod sftp_writer;

use crate::{error::Error, StreamData};
use async_std::channel::Receiver;
use async_trait::async_trait;

#[async_trait]
pub trait StreamWriter {
  async fn write_stream(
    &self,
    path: &str,
    receiver: Receiver<StreamData>,
    job_and_notification: &dyn WriteJob,
  ) -> Result<(), Error>;
}

pub trait WriteJob: Send + Sync {
  fn get_str_job_id(&self) -> String {
    "".to_string()
  }
  fn progress(&self, progress: u8) -> Result<(), Error> {
    log::info!(target: &self.get_str_job_id(), "Received progress {}/100", progress);
    Ok(())
  }

  fn is_stopped(&self) -> bool {
    false
  }
}

pub struct DummyWriteJob {}

impl WriteJob for DummyWriteJob {}
