pub mod endpoint;
mod error;
pub mod list;
pub mod reader;
pub mod secret;
pub mod writer;

pub use error::Error;

#[derive(Debug)]
pub enum StreamData {
  Data(Vec<u8>),
  Size(u64),
  Stop,
  Eof,
}
