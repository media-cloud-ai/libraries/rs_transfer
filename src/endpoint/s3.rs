use crate::{error::Error, secret::S3Secret};
use rusoto_core::HttpClient;
use rusoto_credential::StaticProvider;
use rusoto_s3::S3Client;
use std::{
  convert::TryFrom,
  sync::{Arc, Mutex},
};
use tokio::runtime::Runtime;

#[derive(Clone)]
pub struct S3Endpoint {
  connection: Arc<S3Client>,
  runtime: Arc<Mutex<Runtime>>,
  bucket: String,
}

impl TryFrom<(&S3Secret, Arc<Mutex<Runtime>>)> for S3Endpoint {
  type Error = Error;

  fn try_from((secret, runtime): (&S3Secret, Arc<Mutex<Runtime>>)) -> Result<Self, Self::Error> {
    let connection = Self::connect(secret)?;
    let connection = Arc::new(connection);
    Ok(Self {
      connection,
      runtime,
      bucket: secret.bucket().to_string(),
    })
  }
}

impl S3Endpoint {
  pub fn connect(secret: &S3Secret) -> Result<S3Client, Error> {
    let access_key = secret.access_key();
    let secret_key = secret.secret_key();
    let region = secret.region()?;

    Ok(S3Client::new_with(
      HttpClient::new().map_err(|_| Error::Other("Unable to create HTTP client".to_string()))?,
      StaticProvider::new_minimal(access_key.to_string(), secret_key.to_string()),
      region,
    ))
  }

  pub fn connection(&self) -> Arc<S3Client> {
    self.connection.clone()
  }

  pub fn runtime(&self) -> Arc<Mutex<Runtime>> {
    self.runtime.clone()
  }

  pub fn bucket(&self) -> &str {
    &self.bucket
  }
}
