use crate::secret::CursorSecret;

pub struct CursorEndpoint {
  content: Vec<u8>,
}

impl From<&str> for CursorEndpoint {
  fn from(content: &str) -> Self {
    Self::from(content.as_bytes())
  }
}

impl From<&[u8]> for CursorEndpoint {
  fn from(content: &[u8]) -> Self {
    Self::from(content.to_vec())
  }
}

impl From<Vec<u8>> for CursorEndpoint {
  fn from(content: Vec<u8>) -> Self {
    CursorEndpoint { content }
  }
}

impl From<Option<Vec<u8>>> for CursorEndpoint {
  fn from(content: Option<Vec<u8>>) -> Self {
    CursorEndpoint {
      content: content.unwrap_or_default(),
    }
  }
}

impl From<&CursorSecret> for CursorEndpoint {
  fn from(secret: &CursorSecret) -> Self {
    Self {
      content: secret.content(),
    }
  }
}

impl CursorEndpoint {
  pub fn content(&self) -> &Vec<u8> {
    &self.content
  }
}
