mod cursor;
mod file;
mod ftp;
mod gcs;
mod http;
mod one_drive;
mod s3;
mod sftp;

pub use self::cursor::CursorEndpoint;
pub use self::file::FileEndpoint;
pub use self::ftp::FtpEndpoint;
pub use self::gcs::GcsEndpoint;
pub use self::http::HttpEndpoint;
pub use self::one_drive::OneDriveEndpoint;
pub use self::s3::S3Endpoint;
pub use self::sftp::SftpEndpoint;
