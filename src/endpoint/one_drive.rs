use crate::secret::OneDriveSecret;
use onedrive_api::{DriveLocation, OneDrive};

pub struct OneDriveEndpoint {
  user_id: String,
  token: String,
}

impl From<&OneDriveSecret> for OneDriveEndpoint {
  fn from(secret: &OneDriveSecret) -> Self {
    Self {
      user_id: secret.client_id().to_string(),
      token: secret.token().to_string(),
    }
  }
}

impl OneDriveEndpoint {
  pub fn user_id(&self) -> &str {
    &self.user_id
  }
  pub fn token(&self) -> &str {
    &self.token
  }

  pub fn drive(&self) -> OneDrive {
    OneDrive::new(
      self.token(), // Login token to Microsoft Graph.
      DriveLocation::me(),
    )
  }
}
