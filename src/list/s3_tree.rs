use crate::{
  endpoint::S3Endpoint,
  error::Error,
  list::{strip_relative_path, TreeItem, TreeItemKind, TreeItems, TreeList},
};
use async_trait::async_trait;
use chrono::DateTime;
use rusoto_s3::{CommonPrefix, ListObjectsV2Request, Object, S3};
use std::{convert::TryFrom, time::SystemTime};

const PATH_SEPARATOR: char = '/';

#[async_trait]
impl TreeList for S3Endpoint {
  async fn list(&self, root_path: &str, prefix: Option<&str>) -> Result<TreeItems, Error> {
    let root_path = Self::get_root_path(root_path, PATH_SEPARATOR);
    let absolute_prefix = Self::get_request_prefix(&root_path, prefix);
    let delimiter = Self::get_delimiter(PATH_SEPARATOR);

    let request = ListObjectsV2Request {
      bucket: self.bucket().to_string(),
      delimiter: Some(delimiter),
      prefix: Some(absolute_prefix),
      ..Default::default()
    };

    let client = self.connection();

    let handler = self
      .runtime()
      .lock()
      .unwrap()
      .spawn(async move { client.list_objects_v2(request).await });

    let list_result = handler.await?.map(|response| {
      let mut items =
        Self::parse_response_common_prefixes(response.common_prefixes, self.bucket(), &root_path);

      items.append(&mut Self::parse_response_objects(
        response.contents,
        self.bucket(),
        &root_path,
      ));

      items
    });

    let items = list_result.unwrap_or_else(|error| {
      log::warn!("Cannot access {} content: {:?}", self.bucket(), error);
      TreeItems::default()
    });

    Ok(items)
  }
}

impl S3Endpoint {
  fn get_request_prefix(root_path: &str, prefix: Option<&str>) -> String {
    let absolute_prefix = Self::get_absolute_prefix(root_path, prefix, PATH_SEPARATOR);

    format!(
      "{}{}",
      absolute_prefix.trim_end_matches(PATH_SEPARATOR),
      PATH_SEPARATOR
    )
  }

  fn parse_response_common_prefixes(
    common_prefixes: Option<Vec<CommonPrefix>>,
    bucket: &str,
    prefix: &str,
  ) -> TreeItems {
    common_prefixes
      .map(|common_prefixes| {
        common_prefixes
          .iter()
          .filter_map(move |common_prefix| {
            // println!(" - common_prefix: {:?}", common_prefix);
            let item = TreeItem::try_from((prefix, common_prefix));

            if let Err(error) = &item {
              log::warn!("Cannot access entry in {}: {:?}", bucket, error);
            }

            item.ok()
          })
          .collect()
      })
      .unwrap_or_default()
  }

  fn parse_response_objects(objects: Option<Vec<Object>>, bucket: &str, prefix: &str) -> TreeItems {
    objects
      .map(|objects| {
        Box::new(objects.iter().filter_map(move |object| {
          let item = TreeItem::try_from((prefix, object));

          if let Err(error) = &item {
            log::warn!("Cannot access entry in {}: {:?}", bucket, error);
          }

          item.ok()
        }))
        .collect()
      })
      .unwrap_or_default()
  }
}

impl TryFrom<(&str, &CommonPrefix)> for TreeItem {
  type Error = Error;

  fn try_from((prefix, common_prefix): (&str, &CommonPrefix)) -> Result<Self, Self::Error> {
    let path = common_prefix
      .prefix
      .as_ref()
      .ok_or_else(|| Error::Other(format!("No prefix in common prefix: {common_prefix:?}")))?;

    let path = strip_relative_path(prefix, path, PATH_SEPARATOR);
    let prefix = if prefix == PATH_SEPARATOR.to_string() {
      prefix
    } else {
      prefix.trim_end_matches(PATH_SEPARATOR)
    };

    Ok(Self::new(
      TreeItemKind::Folder,
      &path,
      prefix,
      0,
      SystemTime::UNIX_EPOCH,
      None,
      PATH_SEPARATOR,
    ))
  }
}

impl TryFrom<(&str, &Object)> for TreeItem {
  type Error = Error;

  fn try_from((prefix, object): (&str, &Object)) -> Result<Self, Self::Error> {
    let path = object.key.as_ref().ok_or_else(|| {
      Error::Other(format!(
        "Cannot retrieve 'key' value from S3 object: {object:?}"
      ))
    })?;

    let path = strip_relative_path(prefix, path, PATH_SEPARATOR);
    let prefix = if prefix == PATH_SEPARATOR.to_string() {
      prefix
    } else {
      prefix.trim_end_matches(PATH_SEPARATOR)
    };

    let size = object
      .size
      .as_ref()
      .ok_or_else(|| {
        Error::Other(format!(
          "Cannot retrieve 'size' value from S3 object: {object:?}"
        ))
      })
      .map(|size| *size as u64)?;

    let last_modified = object.last_modified.as_ref().ok_or_else(|| {
      Error::Other(format!(
        "Cannot retrieve 'last_modified' value from S3 object: {object:?}"
      ))
    })?;
    let last_modified = DateTime::parse_from_rfc3339(last_modified)
      .map_err(|error| Error::from((last_modified.as_str(), error)))?;
    let last_update = SystemTime::from(last_modified);

    Ok(Self::new(
      TreeItemKind::File,
      &path,
      prefix,
      size,
      last_update,
      None,
      PATH_SEPARATOR,
    ))
  }
}

#[test]
pub fn test_get_root_path() {
  assert_eq!("/", &S3Endpoint::get_root_path("", PATH_SEPARATOR));
  assert_eq!("/", &S3Endpoint::get_root_path("/", PATH_SEPARATOR));
  assert_eq!("/root", &S3Endpoint::get_root_path("root", PATH_SEPARATOR));
  assert_eq!("/root", &S3Endpoint::get_root_path("root/", PATH_SEPARATOR));
  assert_eq!("/root", &S3Endpoint::get_root_path("/root", PATH_SEPARATOR));
  assert_eq!(
    "/root",
    &S3Endpoint::get_root_path("/root/", PATH_SEPARATOR)
  );
  assert_eq!(
    "/root/path",
    &S3Endpoint::get_root_path("root/path", PATH_SEPARATOR)
  );
  assert_eq!(
    "/root/path",
    &S3Endpoint::get_root_path("root/path/", PATH_SEPARATOR)
  );
  assert_eq!(
    "/root/path",
    &S3Endpoint::get_root_path("/root/path", PATH_SEPARATOR)
  );
  assert_eq!(
    "/root/path",
    &S3Endpoint::get_root_path("/root/path/", PATH_SEPARATOR)
  );
}

#[test]
pub fn test_list_request_prefix() {
  assert_eq!("/", &S3Endpoint::get_request_prefix("", None));
  assert_eq!("/", &S3Endpoint::get_request_prefix("/", None));
  assert_eq!("/root/", &S3Endpoint::get_request_prefix("/root", None));
  assert_eq!(
    "/root/path/",
    &S3Endpoint::get_request_prefix("/root/path", None)
  );

  assert_eq!(
    "/prefix/",
    &S3Endpoint::get_request_prefix("", Some("prefix"))
  );
  assert_eq!(
    "/prefix/",
    &S3Endpoint::get_request_prefix("/", Some("prefix"))
  );
  assert_eq!(
    "/root/prefix/",
    &S3Endpoint::get_request_prefix("/root", Some("prefix"))
  );
  assert_eq!(
    "/root/path/prefix/",
    &S3Endpoint::get_request_prefix("/root/path", Some("prefix"))
  );
}
