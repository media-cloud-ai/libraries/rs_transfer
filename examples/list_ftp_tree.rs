use clap::Parser;
use log::LevelFilter;
use rs_transfer::{endpoint::FtpEndpoint, list::TreeList, secret::FtpSecret};
use std::convert::TryFrom;

#[derive(Parser, Debug)]
struct Args {
  #[clap(short = 'H', long)]
  hostname: String,
  #[clap(short = 'P', long)]
  port: Option<u16>,
  #[clap(short, long)]
  secure: Option<bool>,
  #[clap(short, long)]
  username: Option<String>,
  #[clap(short, long)]
  password: Option<String>,
  #[clap(short, long, default_value = "/")]
  root_path: String,
  #[clap(long)]
  prefix: Option<String>,
}

#[async_std::main]
async fn main() {
  env_logger::builder().filter_level(LevelFilter::Info).init();

  let args = Args::parse();

  let secret = FtpSecret {
    hostname: args.hostname,
    port: args.port,
    secure: args.secure,
    username: args.username,
    password: args.password.map(|value| value.into()),
    root_directory: Some(args.root_path.clone()),
  };

  let ftp_tree = FtpEndpoint::try_from(&secret).unwrap();
  let root_path = args.root_path.as_str();
  let prefix = args.prefix.as_deref();

  println!("\n### List: root_path={root_path}, prefix={prefix:?}");
  {
    let items = ftp_tree.list(root_path, prefix).await.unwrap();
    for item in items {
      println!(" - {item:?}");
    }
  }

  println!("\n### List tree: root_path={root_path}, prefix={prefix:?}");
  {
    let items = ftp_tree.list_tree(root_path, prefix).await.unwrap();
    for item in items {
      println!(" - {item:?}");
    }
  }
}
