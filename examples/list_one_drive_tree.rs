use clap::Parser;
use log::LevelFilter;
use rs_transfer::{endpoint::OneDriveEndpoint, list::TreeList, secret::OneDriveSecret};

#[derive(Parser, Debug)]
struct Args {
  #[clap(short, long)]
  client_id: String,
  #[clap(short, long)]
  token: String,
  #[clap(short, long, default_value = "/")]
  root_path: String,
  #[clap(short, long)]
  prefix: Option<String>,
}

#[async_std::main]
async fn main() {
  env_logger::builder().filter_level(LevelFilter::Info).init();

  let args = Args::parse();

  let secret = OneDriveSecret {
    client_id: args.client_id,
    token: args.token.into(),
  };

  let root_path = args.root_path;

  let endpoint = OneDriveEndpoint::from(&secret);
  let prefix = args.prefix.as_deref();

  println!("\n### List: root_path={root_path}, prefix={prefix:?}");
  {
    let items = endpoint.list(&root_path, prefix).await.unwrap();
    for item in items {
      println!(" - {item:?}");
    }
  }

  println!("\n### List tree: root_path={root_path}, prefix={prefix:?}");
  {
    let items = endpoint.list_tree(&root_path, prefix).await.unwrap();
    for item in items {
      println!(" - {item:?}");
    }
  }
}
