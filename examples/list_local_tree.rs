use clap::Parser;
use log::LevelFilter;
use rs_transfer::{endpoint::FileEndpoint, list::TreeList};

#[derive(Parser, Debug)]
struct Args {
  #[clap(short, long, default_value = "/")]
  root_path: String,
  #[clap(short, long)]
  prefix: Option<String>,
}

#[async_std::main]
async fn main() {
  env_logger::builder().filter_level(LevelFilter::Info).init();

  let args = Args::parse();

  let file_tree = FileEndpoint::default();
  let root_path = args.root_path.as_str();
  let prefix = args.prefix.as_deref();

  println!("\n### List: root_path={root_path}, prefix={prefix:?}");
  {
    let items = file_tree.list(root_path, prefix).await.unwrap();
    for item in items {
      println!(" - {item:?}");
    }
  }

  println!("\n### List tree: root_path={root_path}, prefix={prefix:?}");
  {
    let items = file_tree.list_tree(root_path, prefix).await.unwrap();
    for item in items {
      println!(" - {item:?}");
    }
  }
}
