Rust Transfer
=============

### List of supported remote access to Read/Download data

- [x] Local file
- [x] Download FTP file
- [x] Download SFTP file
- [x] Download HTTP file
- [x] Download S3 file
- [x] Download GCS file

### List of supported remote access to Write/Upload data

- [x] Write local file
- [x] Upload FTP file
- [x] Upload SFTP file
- [x] Upload S3 file
- [x] Upload GCS file
